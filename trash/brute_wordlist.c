#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <openssl/sha.h>

unsigned char obuf[20];

int main(int argc, char **argv)
{
    // int count_threads; // for mpi
    if (argc < 3)
    {
        printf("Compiled with SHA1");
        printf("\nUsage: %s <wordlist> <hash>\n", argv[0]);
        return 1;
    }
    char *filename = (char *)argv[1];
    char *string = (char *)argv[2];
    printf("wordlist: %s\n", filename);
    printf("hash: %s\n", string);
    time_t t0 = time(0);

    FILE *mf;
    char str[50];
    char *estr;
    mf = fopen(filename, "r");
    if (mf == NULL)
    {
        printf("error");
        return -1;
    }
    else
        while (1)
        {
            estr = fgets(str, sizeof(str), mf);
            if (estr == NULL)
            {
                if (feof(mf) != 0)
                {
                    printf("EndFile");
                    break;
                }
                else
                {
                    printf("Error read\n");
                    break;
                }
            }
            //printf("     %s", str);
            SHA1(str, strlen(str), obuf);
            int i;
            // for (i = 0; i < 20; i++)
            // {
            //     printf("%02x", obuf[i]);
            // }
            // printf(": hash\n");

            char passHash[100] = {
                0,
            };
            for (i = 0; i < sizeof(obuf); i++)
            {
                sprintf(passHash + i * 2, "%02x", obuf[i]);
            }
            // printf("%s: passHash\n", passHash);
            // printf("%s: Hash\n", string);

            if (strcmp(string, passHash) == 0) {
                //printf("true\n");
                printf("password: %s", str);
                time_t t1 = time(0);
                double tis = difftime(t1, t0);
                printf("Time: %f\n", tis);
                fclose(mf);
                return 0;
            }
            // else
            //     printf("false\n");
        }

    if (fclose(mf) == EOF)
        printf("File closed\n");

    return 0;
}