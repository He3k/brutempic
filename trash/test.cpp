#include <stdio.h>
#include <mpi.h>

int main(int argc, char* argv[])
{
 int ProcNum, ProcRank;
 int RecvBuf; // ячейка (буфер), через которую поток получает адресованное ему число
 MPI_Status Status;
 MPI_Init (&argc, &argv);
 MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
 MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
 if(ProcRank==0)
 {
 printf("Hello from process \n");
 for(int i=1; i<ProcNum; i++)
 {
// Сообщения с номерами потоков принимаются потоком с №0
// от корреспондентов по мере их готовности
 MPI_Recv(&RecvBuf, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG,
MPI_COMM_WORLD, &Status);
printf("Hello from process \n");
 }
 }
 else
// Сообщения отправляются всеми потоками, кроме нулевого
 MPI_Send(&ProcRank, 1, MPI_INT,0 ,0, MPI_COMM_WORLD);
 MPI_Finalize();
 return 0;
}
