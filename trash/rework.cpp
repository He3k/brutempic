#include <iostream>
#include <string>
#include <vector>
// #include <openssl/md5.h>
#include <openssl/sha.h>
#include <cstring>
#include <mpi.h>
#include <unistd.h>
#include <cmath>

std::vector<char> alphabet({'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9'});
int length = 4;
// unsigned char hash[SHA_DIGEST_LENGTH];
const unsigned char *hash = NULL;
int collision = 0;
/*
void getStartHash()
{
    SHA_CTX sha1handler;
    char buffers[length] = "suka";

    SHA1_Init(&sha1handler);
    SHA1_Update(&sha1handler, buffers, length);
    SHA1_Final(hash, &sha1handler);
}
*/

int getHash(std::string password)
{
    SHA_CTX sha1handler;
    unsigned char sha1digets[SHA_DIGEST_LENGTH];
    const char *buffer = password.c_str();

    SHA1_Init(&sha1handler);
    SHA1_Update(&sha1handler, buffer, length);
    SHA1_Final(sha1digets, &sha1handler);
    /*
    for (int i = 0; i < 20; i++)
    {
        printf("%02x", sha1digets[i]);
    }
    printf(": hash\n");*/
    int i;
    char passHash[100] = {
        0,
    };
    for (i = 0; i < sizeof(sha1digets); i++)
    {
        sprintf(passHash + i * 2, "%02x", sha1digets[i]);
    }
    // printf("%s\n", hash);
    // printf("%s\n", buffer);
    // if (strncmp((char *)hash, (char *)sha1digets, length) == 0)
    // if (strncmp((char *)hash, (char *)sha1digets, length) == 0)
    if (strcmp((char *)hash, (char *)passHash) == 0)
    {
        printf("Password: %s\n", buffer);
        return 1;
    }
    else
        return 0;
}

std::string generatorPassword(std::vector<char> alphabet, int idx, int size)
{
    std::string password(size, alphabet[0]);
    int alphas = alphabet.size();

    while (size--)
    {
        password[size] = alphabet[idx % alphas];
        idx /= alphas;
    }

    return password;
}

void hackingHash(int size, std::vector<char> alphabet)
{
    int commsize, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int c = 0;

    size_t numbers = 1;
    int alphas = alphabet.size();
    numbers = pow(alphas, size);

    size_t numbers_in_proc = numbers / commsize;
    size_t lb = rank * numbers_in_proc;
    size_t ub = (rank == commsize - 1) ? (numbers - 1) : (lb + numbers_in_proc - 1);

    for (size_t i = lb; i <= ub; ++i)
    {
        std::string str = generatorPassword(alphabet, i, size);
        c += getHash(str);
    }

    MPI_Reduce(&c, &collision, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
}

int main(int argc, char *argv[])
{
    int length; // length password
    if (argc < 3)
    {
        printf("Compiled with SHA1");
        printf("\nUsage: %s <password_len> <hash>\n", argv[0]);
        return 1;
    }
    length = atoi(argv[1]); // lenght password
    hash = (const unsigned char *)argv[2];
    printf("length: %d\n", length);
    printf("hash: %s\n", hash);

    //
    int commsize, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // getStartHash();

    double t = MPI_Wtime();
    hackingHash(length, alphabet);
    t = MPI_Wtime() - t;

    if (rank == 0)
    {
        printf("Time (%d procs): %.6f sec.\n", commsize, t);
        printf("Count collision: %d\n", collision);
    }

    MPI_Finalize();
}
